#! /usr/bin/python

__author__ = "avechetti"
__date__ = "$27/10/2015 12:01:23$"

import os
import sys
from pyafipws.wsfev1 import WSFEv1
from pyafipws.wsaa import WSAA


def p_assert_eq(a, b):
    print a, a == b and '==' or '!=', b

def solicitarcae():
    "Funcion principal de pruebas (obtener CAE)"
    
    #import WSFEv1
    
    from ConfigParser import SafeConfigParser
    
    HOMO = False
    DEBUG = '--debug' in sys.argv
    CONFIG_FILE = "rece.ini"
  
    if DEBUG:
        from pysimplesoap.client import __version__ as soapver
        print "pysimplesoap.__version__ = ", soapver
        
    #PERSONALIZACION LIBERTYA 2011-06-30 -- LEER PROPERTIES.INI
#    try:
#    	properties = open("properties.ini")
#    except:
#    	try:
#            open("properties_error.txt", "w").write("No se puede abrir properties.ini")
#        except:
#            sys.exit("Error escribiendo properties_error.txt\n")
#        sys.exit("Error abriendo properties.ini\n")
#
#    try:
#        while True:
#            linea = properties.readline()
#            splited = linea.rsplit("=")
#            name = splited[0]
#            if name == 'CUIT':
#                var_CUIT = splited[1].rstrip()
#                print "CUIT: %s " % splited[1]
#            if name == 'URL_WSAA':
#                WSAAURL = splited[1].rstrip()
#                print "URL_WSAA: %s " % splited[1]
#            if name == 'URL_WSFE':
#                WSFEURL = splited[1].rstrip()
#                print "URL_WSFE: %s " % splited[1]
#            if name == 'CERTIFICADO':
#                CERTIFICADO = splited[1].rstrip()
#                print "CERTIFICADO: %s " % splited[1]
#            if name == 'KEY':
#                KEY = splited[1].rstrip()
#                print "KEY: %s " % splited[1]                          
#            if not linea: break
#    except:
#        try:
#            open("error.txt", "w").write("Faltan parametros en el archivo properties.ini")
#        except:
#            sys.exit("Error escribiendo error.txt\n")
#        sys.exit("Faltan parametros en el archivo properties.ini")
    #FIN LEER PROPERTIES
    
#   Leer Configuracion del archivo rece.ini

    if len(sys.argv) > 1 and sys.argv[1][0] not in "-/":
        CONFIG_FILE = sys.argv.pop(1)
    if DEBUG: print "CONFIG_FILE:", CONFIG_FILE   
   
    config = SafeConfigParser()
    config.read(CONFIG_FILE)
    #print CONFIG_FILE
    CERTIFICADO = config.get('WSAA', 'CERT')
    KEY = config.get('WSAA', 'PRIVATEKEY')
    cuit = config.get('WSFEv1', 'CUIT')
    
    entrada = config.get('WSFEv1', 'ENTRADA')
    salida = config.get('WSFEv1', 'SALIDA')
    
    if config.has_option('WSAA', 'URL') and not HOMO:
        wsaa_url = config.get('WSAA', 'URL')
    else:
        wsaa_url = None
    
    if config.has_option('WSFEv1', 'URL') and not HOMO:
        wsfev1_url = config.get('WSFEv1', 'URL')
    else:
        wsfev1_url = None

    if config.has_option('WSFEv1', 'REPROCESAR'):
        wsfev1_reprocesar = config.get('WSFEv1', 'REPROCESAR') == 'S'
    else:
        wsfev1_reprocesar = None

    if config.has_section('PROXY') and not HOMO:
        proxy_dict = dict(("proxy_%s" % k, v) for k, v in config.items('PROXY'))
        proxy_dict['proxy_port'] = int(proxy_dict['proxy_port'])
    else:
        proxy_dict = {}
    
    cacert = config.has_option('WSFEv1', 'CACERT') and config.get('WSFEv1', 'CACERT') or None
    wrapper = config.has_option('WSFEv1', 'WRAPPER') and config.get('WSFEv1', 'WRAPPER') or None

    if DEBUG:
        print "wsaa_url %s\nwsfev1_url %s\ncuit %s" % (wsaa_url, wsfev1_url, cuit)
        if proxy_dict: print "proxy_dict=", proxy_dict

    if '/x' in sys.argv:
        escribir_facturas([{'err_msg': "Prueba",
                          }], open("x.txt", "w"))
   
       
    pwd = os.getcwd()
    print "Directorio Actual ", pwd 
    
    
    # VerificaciOn del web server remoto, necesario para verificar canal seguro
    #CACERT = os.path.join(pwd, "conf/afip_ca_info.crt") # WSAA CA Cert (Autoridades de Confiaza)
    
    crt_path = os.path.join(pwd, CERTIFICADO)
    print crt_path
    
    key_path = os.path.join(pwd, KEY)
    print key_path 
    
    cache = os.path.join(pwd, "cache")
    wrapper = None
    service  = "wsfe"
    proxy = None
    
    wsaa = WSAA()
    wsaa.LanzarExcepciones = True
    
    ta = wsaa.Autenticar(service, crt_path, key_path, wsaa_url, proxy, wrapper, cacert)
 
    wsfev1 = WSFEv1()
   
    wsfev1.Conectar(cache, wsfev1_url, proxy, wrapper, cacert)

    if DEBUG:
        print "LOG: ", wsfev1.DebugLog()
        
    if "--dummy" in sys.argv:
        print wsfev1.client.help("FEDummy")
        wsfev1.Dummy()
        print "AppServerStatus", wsfev1.AppServerStatus
        print "DbServerStatus", wsfev1.DbServerStatus
        print "AuthServerStatus", wsfev1.AuthServerStatus
        sys.exit(0)


#    # obteniendo el TA
#    TA = "TA.xml"
#    if 'wsaa' in sys.argv or not os.path.exists(TA) or os.path.getmtime(TA) + (60 * 60 * 5) < time.time():
#        import wsaa
#        tra = wsaa.create_tra(service="wsfe")
#        #cms = wsaa.sign_tra(tra,"hiperg_17abe98f029c75bf.crt","tehuelche.key")
#        cms = wsaa.sign_tra(tra, CERTIFICADO, KEY)
#        url = "" # "https://wsaa.afip.gov.ar/ws/services/LoginCms"
#        ta_string = wsaa.call_wsaa(cms, url)
#        open(TA, "w").write(ta_string)
#    ta_string = open(TA).read()
#    ta = SimpleXMLElement(ta_string)
#    # fin TA
    

#    if '--cuit' in sys.argv:
#        cuit = sys.argv[sys.argv.index("--cuit")+1]
#    else:
#        cuit = "33504487119"

    #TIPOS DE COMPROBANTES
    #   === Tipos de Comprobante ===
    #1: Factura A (20100917-NULL)
    #2: Nota de Debito A (20100917-NULL)
    #3: Nota de Credito A (20100917-NULL)
    #6: Factura B (20100917-NULL)
    #7: Nota de Debito B (20100917-NULL)
    #8: Nota de Credito B (20100917-NULL)
    #4: Recibos A (20100917-NULL)
    #5: Notas de Venta al contado A (20100917-NULL)
    #9: Recibos B (20100917-NULL)
    #10: Notas de Venta al contado B (20100917-NULL)
    #63: Liquidacion A (20100917-NULL)
    #64: Liquidacion B (20100917-NULL)
    #34: Cbtes. A del Anexo I, Apartado A,inc.f),R.G.Nro. 1415 (20100917-NULL)
    #35: Cbtes. B del Anexo I,Apartado A,inc. f),R.G. Nro. 1415 (20100917-NULL)
    #39: Otros comprobantes A que cumplan con R.G.Nro. 1415 (20100917-NULL)
    #40: Otros comprobantes B que cumplan con R.G.Nro. 1415 (20100917-NULL)
    #60: Cta de Vta y Liquido prod. A (20100917-NULL)
    #61: Cta de Vta y Liquido prod. B (20100917-NULL)
    #11: Factura C (20110426-NULL)
    #12: Nota de Debito C (20110426-NULL)
    #13: Nota de Credito C (20110426-NULL)
    #15: Recibo C (20110426-NULL)
    #FIN TIPOS DE COMPROBANTES

    #PERSONALIZACION LIBERTYA 2011-06-06
    try:
    	entrada = open("entrada.txt")
    except:
        try:
            open("error.txt", "w").write("Error abriendo entrada.txt")
        except:
            sys.exit("Error escribiendo error.txt\n")
        sys.exit("Error abriendo entrada.txt\n")

    var_numero = entrada.readline()
    var_punto_vta = entrada.readline()
    var_tipo_cbte = entrada.readline()
    var_tipo_doc = entrada.readline()
    var_nro_doc = entrada.readline()
    var_imp_total = entrada.readline()
    var_imp_neto = entrada.readline()
    var_fecha_cbte = entrada.readline().strip()
    var_presta_serv = entrada.readline()
    var_moneda = entrada.readline()
    var_cotizacion = entrada.readline()
    var_impuestos = entrada.readline().split(";")
    var_percepciones = entrada.readline().split(";")
    var_total_percepciones = entrada.readline()
    
    #FIN
    
    wsfev1.Cuit = cuit
    wsfev1.SetTicketAcceso(ta)
    
    #ACTUALIZADO - DISYTEL - 30-06-2011
    
    if "--archivo" in sys.argv:
        print "wsfev1.client.help:", wsfev1.client.help("FECAESolicitar").encode("latin1")

        tipo_cbte = var_tipo_cbte
        punto_vta = var_punto_vta
        
        #Se obtiene el ultimo comprobante.
        cbte_nro_ult = long(wsfev1.CompUltimoAutorizado(tipo_cbte, punto_vta) or 0)
        print "ult_cbte_nro", cbte_nro_ult
        
        
        fecha = var_fecha_cbte
        concepto = 2
        tipo_doc = var_tipo_doc
        nro_doc = var_nro_doc
        cbt_desde = cbte_nro_ult + 1; 
        cbt_hasta = cbte_nro_ult + 1
        imp_total = float(var_imp_total)
        imp_tot_conc = "0.00"
        #imp_neto = "1.00"
        imp_neto = float(var_imp_neto)
        #imp_iva = "0.21"
	imp_trib = var_total_percepciones
        imp_iva = float(var_imp_total) - float(var_imp_neto) - float(var_total_percepciones)
        print "imp_total", imp_total
        print "imp_neto", imp_neto
        print "imp_iva", imp_iva
        imp_op_ex = "0.00"
        fecha_cbte = fecha; fecha_venc_pago = fecha
        # Fechas del periodo del servicio facturado (solo si concepto = 1?)
        fecha_serv_desde = fecha; fecha_serv_hasta = fecha
        moneda_id = var_moneda.replace('\n', '').strip()
        #moneda_ctz = '1.000'
        moneda_ctz = var_cotizacion

        wsfev1.CrearFactura(concepto, tipo_doc, nro_doc, tipo_cbte, punto_vta, 
                            cbt_desde, cbt_hasta, imp_total, imp_tot_conc, imp_neto,
                            imp_iva, imp_trib, imp_op_ex, fecha_cbte, fecha_venc_pago, 
                            fecha_serv_desde, fecha_serv_hasta, moneda_id, moneda_ctz)
        
#        if tipo_cbte not in (1, 2):
#            tipo = 19
#            pto_vta = 2
#            nro = 1234
#            wsfev1.AgregarCmpAsoc(tipo, pto_vta, nro)
        
#        id = 99
#        desc = 'Impuesto Municipal Matanza'
#        base_imp = 100
#        alic = 1
#        importe = 1
#        wsfev1.AgregarTributo(id, desc, base_imp, alic, importe)

    i = 0
    while i < len(var_impuestos):
        datos = var_impuestos[i].split(":")
        id = datos[0]
        base_imp = datos[1]
        importe = datos[2]
        wsfev1.AgregarIva(id, base_imp, importe)
        i = i + 1

    if (float(var_total_percepciones) > 0):
        i = 0
        while i < len(var_percepciones):
            datos = var_percepciones[i].split(":")
            id = datos[0]
            base_imp = datos[1]
            importe = datos[2]
            alic = datos[3]
            wsfev1.AgregarTributo(id, "Tributo", base_imp, alic, importe)
            i = i + 1
                
    #id = 5 # 21%
    #base_imp = 1
    #importe = 0.21
    #wsfev1.AgregarIva(id, base_imp, importe)
        
    import time
    t0 = time.time()
    wsfev1.CAESolicitar()
    t1 = time.time()
        
    print "Resultado", wsfev1.Resultado
    print "Reproceso", wsfev1.Reproceso
    print "CAE", wsfev1.CAE
    if DEBUG:
        print "t0", t0
        print "t1", t1
        print "lapso", t1-t0
        open("xmlrequest.xml", "wb").write(wsfev1.XmlRequest)
        open("xmlresponse.xml", "wb").write(wsfev1.XmlResponse)

    wsfev1.AnalizarXml("XmlResponse")
    p_assert_eq(wsfev1.ObtenerTagXml('CAE'), str(wsfev1.CAE))
    p_assert_eq(wsfev1.ObtenerTagXml('Concepto'), '2')
    p_assert_eq(wsfev1.ObtenerTagXml('Obs', 0, 'Code'), "10063")
        
    var_msg = str(wsfev1.ObtenerTagXml('Obs', 0, 'Msg'))
    if var_msg == "None":
        var_msg = str(wsfev1.ObtenerTagXml('Err', 0, 'Msg'))

    var_msg = var_msg.replace(':', '=')

    try:
        salida = str(wsfev1.Resultado) + ":" + str(wsfev1.CAE) + ":" + str(cbt_desde) + ":" + var_msg + ":" + str(fecha_cbte)
        print "salida: %s " % salida
        open("salida.txt", "w").write(salida)
        print "El archivo salida.txt se ha generado correctamente."
    except:
        sys.exit("Error escribiendo salida.txt\n")





solicitarcae()
